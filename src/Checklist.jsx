import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';

function Checklist() {
    const [data, setData] = useState([]);
    const { id } = useParams();

    useEffect(() => {
        axios.get(`https://greenvelvet.alwaysdata.net/pfc/${id}`)
            .then(res => setData(res.data))
            .catch(err => console.log(err));
    }, [id]);

    const handleToggleTask = (taskId) => {
        const updatedData = { ...data };
        const updatedTasks = updatedData.tasks.map(task => {
            if (task.id === taskId) {
                task.isDone = !task.isDone;
            }
            return task;
        });

        updatedData.tasks = updatedTasks;

        const allTasksDone = updatedTasks.every(task => task.isDone);
        updatedData.status = allTasksDone ? "terminée" : "en cours";

        setData(updatedData);

        axios.put(`https://greenvelvet.alwaysdata.net/pfc/${id}`, updatedData)
            .then(res => console.log('Task status updated successfully'))
            .catch(err => console.log(err));
    };

    return (
        <div className='d-flex w-100 vh-100 justify-content-center align-items-center bg-ligth vw-100'>
            <div className='w-50 border bg-white shadow px-5 pt-3 pb-5 rounded'>
                <h3>Checklist</h3>
                <div className='mb-2'>
                    <strong>Title: {data.title}</strong>
                    <div className='mb-2'>
                        <strong>Description: {data.description}</strong>
                    </div>
                    <div className='mb-2'>
                        <strong>State: {data.status}</strong>
                    </div>
                    <h5>Tasks:</h5>
                    <ul>
    {data.tasks && data.tasks.map(task => (
        <li key={task.id} style={{ textDecoration: task.isDone ? 'line-through' : 'none' }}>
            {task.title}
            <button onClick={() => handleToggleTask(task.id)} className='btn btn-sm btn-info ms-2'>
                Toggle
            </button>
        </li>
    ))}
</ul>
                    <Link to='/' className='btn btn-success'>Back to Dashboard</Link>
                </div>
            </div>
        </div>
    );
}

export default Checklist;
