import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function Dashboard() {
    const [data, setData] = useState([
        {
            id: 1,
            title: 'Title1',
            description: 'Description 1',
            status: 'En cours',
            tasksDone: 2,
            totalTasks: 5,
            tasks: [
                { id: 1, name: 'Task 1', completed: true },
                { id: 2, name: 'Task 1', completed: false },
                { id: 3, name: 'Task 1', completed: true },
            ],
        },
    ]);

    useEffect(() => {
        const token = 'ca49d5e17bfd7a81ef83f6dd62eee8ac63a4e16a';
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

        axios.get('https://greenvelvet.alwaysdata.net/pfc/checklists')
            .then(res => setData(res.data))
            .catch(err => console.log(err));
    }, []);

    const handleDelete = (id) => {
        const confirmDelete = window.confirm("Would you like to delete?");
        if (confirmDelete) {
            axios.get('https://greenvelvet.alwaysdata.net/pfc/checklist/delete' + id)
                .then(res => {
                    const updatedData = data.filter(item => item.id !== id);
                    setData(updatedData);
                })
                .catch(err => console.log(err));
        }
    }

    return (
        <div className='d-flex flex-column justify-content-center align-items-center bg-light vh-100 vw-100'>
        <h1>Dashboard</h1>
        <div className='w-75 rounded bg-white border shadow p-4'>
            <div className='d-flex justify-content-end'>
                <Link to="/form/new" className='btn btn-success'>Add</Link>
            </div>
            
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Tasks</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
    {data.length > 0 ? (
        data.map((checklist, i) => (
            <tr key={i}>
                <td>{checklist.title}</td>
                <td>{checklist.description}</td>
                <td>{checklist.status}</td>
                <td>{`${checklist.tasksDone} / ${checklist.totalTasks}`}</td>
                <td>
                    <Link to={`/checklist/${checklist.id}`} className='btn btn-sm btn-info me-2'>View</Link>
                    <Link to={`/form/${checklist.id}`} className='btn btn-sm btn-primary me-2'>Edit</Link>
                    <button onClick={() => handleDelete(checklist.id)} className='btn btn-sm btn-danger'>Delete</button>

                    <div>
                        <h5>Tasks:</h5>
                        <ul>
                            {checklist.tasks.map((task, j) => (
                                <li key={j}>{task.name} - {task.completed ? 'Completed' : 'Incomplete'}</li>
                            ))}
                        </ul>
                    </div>
                </td>
            </tr>
        ))
    ) : (
        <tr>
            <td colSpan="5">No data available</td>
        </tr>
    )}
</tbody>
                </table>
            </div>
        </div>
        
    );
}

export default Dashboard;
