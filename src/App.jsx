import React from 'react'
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Dashboard from './Dashboard'
import Form from './Form'
import Checklist from './Checklist'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
   <BrowserRouter>
   <Routes>
   <Route path='/' element ={ <Dashboard />}></Route>
   <Route path='/form/:id' element ={ <Form />}></Route>
   <Route path='/checklist/:id' element ={ <Checklist />}></Route>
 
    </Routes>
    </BrowserRouter>
  )
}

export default App