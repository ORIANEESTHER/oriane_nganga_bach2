import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

function Form() {
    const [formData, setFormData] = useState({
        title: '',
        description: '',
        tasks: [],
    });

    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        if (id) {
            const token = 'ca49d5e17bfd7a81ef83f6dd62eee8ac63a4e16a';
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

            axios.get(`https://greenvelvet.alwaysdata.net/pfc/checklist/add${id}`)
                .then(res => setFormData(res.data))
                .catch(err => console.log(err));
        }
    }, [id]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleAddTask = () => {
        setFormData({
            ...formData,
            tasks: [...formData.tasks, { id: Date.now(), title: '', isDone: false }],
        });
    };

    const handleRemoveTask = (taskId) => {
        setFormData({
            ...formData,
            tasks: formData.tasks.filter(task => task.id !== taskId),
        });
    };

    const handleTaskChange = (taskId, value) => {
        setFormData({
            ...formData,
            tasks: formData.tasks.map(task => {
                if (task.id === taskId) {
                    task.title = value;
                }
                return task;
            }),
        });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const endpoint = id ? `https://greenvelvet.alwaysdata.net/pfc/checklist/add${id}` : 'https://greenvelvet.alwaysdata.net/pfc/checklist/add';

        axios.post(endpoint, formData)
            .then(res => {
                console.log(res);
                navigate('/');
            })
            .catch(err => console.log(err));
    };

    return (
        <div className='d-flex w-100 vh-100 justify-content-center align-items-center bg-light vw-100'>
            <div className='w-50 border bg-white shadow px-5 pt-3 pb-5 rounded'>
                <h1>{id ? 'Edit Checklist' : 'Add Checklist'}</h1>
                <form onSubmit={handleSubmit}>
                    <div className='mb-2'>
                        <label htmlFor='title'>Titre :</label>
                        <input
                            type='text'
                            name='title'
                            className='form-control'
                            placeholder='Enter title'
                            value={formData.title}
                            onChange={handleInputChange}
                        />
                    </div>
                    <div className='mb-2'>
                        <label htmlFor='description'>Description :</label>
                        <input
                            type='text'
                            name='description'
                            className='form-control'
                            placeholder='Enter description'
                            value={formData.description}
                            onChange={handleInputChange}
                        />
                    </div>
                    <h5>Tasks:</h5>
                    {formData.tasks.map(task => (
                        <div key={task.id} className='mb-2'>
                            <input
                                type='text'
                                className='form-control'
                                placeholder='Enter task'
                                value={task.title}
                                onChange={(e) => handleTaskChange(task.id, e.target.value)}
                            />
                            <button type='button' onClick={() => handleRemoveTask(task.id)} className='btn btn-danger ms-2'>
                                Remove Task
                            </button>
                        </div>
                    ))}
                    <button type='button' onClick={handleAddTask} className='btn btn-success'>
                        Add Task
                    </button>
                    <button type='submit' className='btn btn-success ms-3'>
                        Save
                    </button>
                    <Link to='/' className='btn btn-primary ms-3'>
                        Back to Dashboard
                    </Link>
                </form>
            </div>
        </div>
    );
}

export default Form;
